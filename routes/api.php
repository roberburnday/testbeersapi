<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BeerController; //app/Http/Controllers/BeerController.php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('client')->post('/beer/list', [BeerController::class, 'getBeerList']);
Route::middleware('client')->get('/beer/{id}', [BeerController::class, 'getBeerByID']);
Route::middleware('client')->post('/beer/create', [BeerController::class, 'createBeer']);
Route::middleware('client')->put('/beer/update/{beer}', [BeerController::class, 'updateBeer']);
Route::middleware('client')->post('/beer/set/ingredientsto/{beer}', [BeerController::class, 'setIngredients']);
