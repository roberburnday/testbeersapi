<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Beers;
use App\Models\Ingredients;
use Illuminate\Support\Facades\DB;

use Exception;

class BeerController extends Controller
{

    //TODO: generar los requests fuera del controller
    //TODO: extraer funcionalidad en el service

    private $filters = [
        "name","alcohol_graduation","tipe","gluten","price"
    ];

    /**
     * Retorna un listado JSON de todas las cervezas
     * @param $request Filtros y order
     */
    public function getBeerList(Request $request){
        //filtrar
        $beers = new Beers();
        if(count($request->all()) !== 0){
            foreach ($request->all() as $field => $filter) {
                if(in_array($field, $this->filters)){
                    $beers = $beers->where($field,$filter);
                }
            }
        }
        //ordenar
        $order = $request->input("orderField");
        $orderDirection = 
            $request->input("orderDirection") ? $request->input("orderDirection") : "DESC";
        if($order){
            $beers = $beers->orderBy($order, $orderDirection);
        }
        //le ponemos que recoja los ingredientes
        $beers = $beers->with('ingredients');
        //retornamos los datos
        $beers = $beers->get();
        //retornamos el resultado
        return response()->json($beers, 200);
    }

    /**
     * Retorna una única cerveza
     * @param $id El id por el que vamos a retornar la cerveza
     */
    public function getBeerByID($id){
        //conseguimos la info
        $beer = Beers::where('id',$id)->with('ingredients')->firstOrFail();
        return response()->json($beer, 200);
    }

    /**
     * Creamos una nueva cerveza
     * @param $request Datos de la petición
     */
    public function createBeer(Request $request){
        $beer = new Beers();
        //checkear los datos //TODO: con los requests fuera este paso es transparente
        $validated = $request->validate($beer->validator());
        $inserted = true;
        $id_last_inserted = null;
        try{
            //setear $beer + save
            $beer->fill($request->all());
            $beer->save();
            //recogemos el id
            $id_last_inserted = $beer->id;
        }catch(Exception $e){
            $inserted = false;
        }
        //mostramos el resultado
        return response()->json([
            'status' => $inserted,
            'id' => $id_last_inserted
        ],200);
    }

    /**
     * Editamos una nueva cerveza
     * @param $request Datos de la petición
     */
    public function updateBeer(Request $request, Beers $beer){
        $beer = new Beers();
        $validated = $request->validate($beer->validator('PUT'));
        $inserted = true;
        try{
            $beer->update($request->toArray()); //TODO: format post insert --> validator
        }catch(Exception $e){
            $inserted = false;
        }
        //validate el $request
        return response()->json([
            'status' => $inserted
        ],200);
    }

    /**
     * Se setean los ingredientes para la cerveza
     * @param $request Datos para setear ingredientes a la cerveza
     */
    public function setIngredients(Request $request, Beers $beer){
        $validated = $request->validate([
            'ingredients_id' => 'required'
        ]);
        //comprobamos que los id's de los ingredientes existan
        DB::beginTransaction();
        $updated = true;
        try{
            $array_ingredients = explode(",",$request->ingredients_id);
            $ingredients = Ingredients::whereIn('id',$array_ingredients)->pluck('id')->toArray();
            
            //sync es mucho más intuitivo para el programador que lo hereda
            if(count($ingredients) === 0){
                throw new Exception("no ingredients valids");    
            }else{
                $beer->ingredients()->sync($ingredients);  
            }
            //commiteamos los cambios
            DB::commit();
        }catch(\Exception $e){
            //revertimos los datos por el error
            DB::rollBack();
            $updated = false;
        }
        //return del json
        if($updated){
            return response()->json([
                'status' => $updated,
                'beer' => Beers::with('ingredients')->findOrFail($request->beer_id)
            ], 200);
        }else{
            return response()->json([
                'status' => $updated
            ], 422);
        }
    }
}
