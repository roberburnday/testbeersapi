<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Ingredients;
use Illuminate\Http\Request;


class Beers extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'alcohol_graduation',
        'tipe',
        'gluten',
        'price'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function ingredients(){
        return $this->belongsToMany(
            Ingredients::class, 
            'beerIngredients', //tabla pivote/intermedia
            'beer_id', 
            'ingredient_id' 
        );
    }

    public function validator($method = "POST"){
        //construimos el objeto de validacion
        $validate = [
            'name' => 'required|string|max:100',
            'alcohol_graduation' => 'required|numeric|max:100',
            'tipe' => [
                'required',
                'in:IPA,Black,Lagger,Pilsen',
             ],
            'gluten' => 'boolean',
            'price' => 'required|numeric|max:100',
        ];
        switch ($method){
            case "PUT":
            case "PATCH":
                $validate['id'] = 'required|numeric';
                $validate['name'] = 'string|max:100';
                $validate['alcohol_graduation'] = 'numeric|max:100';
                $validate['tipe'] = 'in:IPA,Black,Lagger,Pilsen';
                $validate['price'] = 'numeric|max:10';
                break;
        }
        //return $validate
        return $validate;
    }
}
