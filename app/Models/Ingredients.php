<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Models\Beers;

class Ingredients extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function beers(){
        return $this->belongsToMany(
            Ingredients::class, 
            'beerIngredients', //tabla pivote/intermedia 
            'ingredient_id',
            'beer_id'
        );
    }
}
