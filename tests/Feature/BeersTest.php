<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\Beers;
use App\Models\Ingredients;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class BeersTest extends TestCase
{
    //TODO: remake beers test

    /**
     * A basic test.
     *
     * @return void
     */
    private function getToken()
    {
        //conseguimos de la tabla oauth_clients el id 3
        //si no existe lo creamos
        $userApi = DB::table('oauth_clients')->where('id',3)->get();
        if(count($userApi->toArray()) == 0){
            //creamos el usuario api y recogemos el usuario
            $userApi = DB::table('oauth_clients')->insert([
                'id' => 3,
                'name' => "UserApi",
                'secret' => "h4aBVPVdOp1cfA2YeQDBhPZsqb9VaD9uRMgTfRlH",
                'redirect' => "http://localhost/auth/callback",
                'personal_access_client' => 0,
                'password_client' => 0,
                'revoked' => 0,
            ]);
            //recogemos el resultado
            $userApi = DB::table('oauth_clients')->where('id',3)->get();
        }
        $id = $userApi[0]->id;
        $client = $userApi[0]->secret;
        //con las credenciales forzamos el generate token
        $body = [
            'client_id' => $id,
            'client_secret' => $client,
            'grant_type' => 'client_credentials',
            'scope' => ''
        ];
        //generamos el token y lo retornamos
        $response = $this->json('POST','/oauth/token',$body,['Accept' => 'application/json']);
        $response->assertStatus(200);
        return $response->json("token_type")." ".$response->json("access_token");
    }

    /**
     * A basic test.
     *
     * @return void
     */
    public function test_createbeer()
    {
        $token = $this->getToken();
        //test OK beer
        $beer = Beers::factory()->make(["tipe"=>"Pilsen"])->toArray();
        $this->json('POST','/api/beer/create/',$beer,[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])
        ->assertStatus(200);
        
        //test FAIL beer
        $beer = Beers::factory()->make(["tipe"=>"Pilsen_fake"])->toArray();
        $this->json('POST','/api/beer/create/',$beer,[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])
        ->assertStatus(422);
        
    }

    /**
     * A basic test.
     *
     * @return void
     */
    public function test_updatebeer()
    {
        $token = $this->getToken();
        $beer = Beers::first();
        $beer->name = "fake ".Carbon::now();
        //update OK
        $beer->tipe = "Black";
        $this->json('POST','/api/beer/update/'.$beer->id,$beer->toArray(),[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])
        ->assertStatus(200);
        //update False
        $beer->tipe = "Black_false";
        $this->json('POST','/api/beer/update/'.$beer->id,$beer->toArray(),[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])
        ->assertStatus(422);
    }

    /**
     * A basic test.
     *
     * @return void
     */
    public function test_associatebeerstoingredients()
    {
        //TODO: con la function setIngredients
        $token = $this->getToken();
        //coger 3 ingredientes 
        $ingredients = Ingredients::limit(3)->pluck('id')->toArray();
        //coger una cerveza
        $beer = Beers::first();
        //llamar al metodo post - OK
        $this->json('POST','/api/beer/set/ingredientsto/'.$beer->id,[
            'ingredients_id' => implode(",", $ingredients)
        ],[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])
        ->assertStatus(200);
        //->assertJsonStructure([]);
        //llamar al metodo post - fail
        $this->json('POST','/api/beer/set/ingredientsto/9999999999999',[
            'ingredients_id' => implode(",", $ingredients)
        ],[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])
        ->assertStatus(404);
        //llamar al metodo post - fail
        $this->json('POST','/api/beer/set/ingredientsto/'.$beer->id,[
            //TODO: para evitar que el dato exista, habrá que pillar el id más grande y sumarle uno
            'ingredients_id' => '77777777,888888888888,9999999999,10000000'
        ],[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])
        ->assertStatus(422);
    }

    /**
     * A basic test.
     *
     * @return void
     */
    public function test_get()
    {
        $token = $this->getToken();
        $beer = Beers::first();
        //get beer by ID OK
        $this->json('GET','/api/beer/'.$beer->id,[],[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])->assertStatus(200);
        //get beer by ID 404 - not found
        $this->json('GET','/api/beer/9999999',[],[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])->assertStatus(404);
        //get beer list filter -- OK
        $this->json('POST','/api/beer/list',[],[
            'Accept' => 'application/json',
            'Authorization' => $token,
        ])->assertStatus(200);
    }
}
