<?php

namespace Database\Factories;

use App\Models\Beers;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class BeersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Beers::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name'               => $this->faker->regexify('[A-Za-z0-9]{10}'),  //asciify('********'),
            'alcohol_graduation' => $this->faker->randomFloat(2, 0, 100),
            'tipe'               => $this->faker->randomElement(['IPA', 'Black','Lagger','Pilsen']),
            'gluten'             => $this->faker->boolean(),
            'price'              => $this->faker->randomFloat(2, 0, 15),
        ];
    }
}
