<?php

namespace Database\Factories;

use App\Models\Ingredients;
use Illuminate\Database\Eloquent\Factories\Factory;

class IngredientsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ingredients::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->randomElement(['Grano', 'Resina','Trazas','Quimico'])
                ." ".$this->faker->randomElement(['de Cebada', 'de Pino','554b3',
                'de Trigo','de SubCebada', 'de Aceituna', '44r9', 'de Metal']),
            'description' => $this->faker->text(101),
        ];
    }
}
