<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beers', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable(false);
            $table->float("alcohol_graduation")->nullable(false);
            $table->enum('tipe', ['IPA', 'Black','Lagger','Pilsen'])->nullable(false);
            $table->boolean('gluten',true);
            $table->float('price')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beers');
        //To production: rename
    }
}
