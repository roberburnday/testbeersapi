<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BeerIngredients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('beerIngredients', function (Blueprint $table) {
            //creamos campos para poder relacionar las 2 tablas existentes
            $table->unsignedBigInteger('beer_id'); //user_id
            $table->unsignedBigInteger('ingredient_id'); //role_id

            //creamos el fk (foering key) usando los campos anteriores
            $table->foreign('beer_id')->references('id')->on('beers');
            $table->foreign('ingredient_id')->references('id')->on('ingredients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beerIngredients');
        //To production: rename
    }
}
