<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Beers;
use App\Models\Ingredients;

//use database\factories\BeerFactory;

class CreateBeers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Generamos los ingredientes
        Ingredients::factory()->count(20)->create();
        //Generamos 5 cervezas y les asignamos ingredientes
        Beers::factory()->count(5)->create()->each(function ($beer) {
            //recogemos los ingredientes que va a tener nuestra cerveza
            $ingredients = Ingredients::orderBy(DB::raw('RAND()'))
            ->limit(5)->pluck('id');
            //almacenamos las referencias para insertarlas en un unico insert batch
            
            foreach ($ingredients as $ingredient) {
                $beer->ingredients()->sync($ingredients);
            }
        });
    }
}
